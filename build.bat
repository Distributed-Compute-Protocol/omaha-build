@echo off
REM
REM @file         build.bat
REM               Build the Omaha library, assuming that the working directory
REM               is the Omaha source root directory.
REM @author       Jason Erb, jason@distributive.network
REM @date         September 2021

setLocal enableExtensions

call "%~dp0env.cmd"

pushd omaha
hammer %*
popd
