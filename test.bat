@echo off
REM
REM @file         test.bat
REM               Test the Omaha library, assuming that the working directory
REM               is the configuration subdirectory of the build directory.
REM @author       Jason Erb, jason@distributive.network
REM @date         September 2021

setLocal enableExtensions

net session >NUL
if %ERRORLEVEL% NEQ 0 (
  echo Tests must be run as Administrator.
  exit
)

call "%~dp0env.cmd"

set "OMAHA_PSEXEC_DIR=%ProgramFiles(x86)%\pstools"
set "OMAHA_TEST_BUILD_SYSTEM=1"

reg add "HKLM\SOFTWARE\WOW6432Node\Distributive\UpdateDev" /v TestSource /t REG_SZ /d ossdev /f

pushd staging
omaha_unittest %*
popd
