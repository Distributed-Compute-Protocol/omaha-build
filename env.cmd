@echo off
REM
REM @file         env.cmd
REM               Set up the Omaha build and test environment.
REM @author       Jason Erb, jason@distributive.network
REM @date         September 2021

set "PATH=C:\Python27;%PATH%"
set "GOROOT=%ProgramFiles%\Go"

set "WindowsSdkDir=%ProgramFiles(x86)%\Windows Kits\10\"
set "WindowsSDKVersion=10.0.18362.0"
set "WindowsSdkVerBinPath=%WindowsSdkDir%bin\%WindowsSDKVersion%"
set "WindowsSDKVersion=%WindowsSDKVersion%\"

set "TIMESTAMP_SERVER=http://timestamp.digicert.com"
set "SHA1_TIMESTAMP_SERVER=%TIMESTAMP_SERVER%"
set "SHA2_TIMESTAMP_SERVER=%TIMESTAMP_SERVER%"

set "OMAHA_PROTOBUF_SRC_DIR=%~dp0submodules\protobuf\src"
set "OMAHA_WTL_DIR=%~dp0submodules\wtl"
set "SCT_DIR=%~dp0submodules\swtoolkit"

:: Change "Community" to "Professional", etc., if using a non-community version
:: of VS.
call "%ProgramFiles(x86)%\Microsoft Visual Studio\2019\Community\VC\Auxiliary\Build\vcvarsamd64_x86.bat"
