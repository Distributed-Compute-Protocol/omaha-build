# INTRODUCTION

This repository contains scripts to build and release Omaha binaries.

These instructions complement the original developer notes. They are targeted at
setting up a stand-alone build machine. The primary assumption is that one is
starting with a basic Windows installation with no other software installed.

The scripts associated with these instructions will download the necessary
packages from the internet and install them.

Auxiliary Documents:

* [Developer Setup Guide](https://github.com/Kings-Distributed-Systems/omaha/blob/master/doc/DeveloperSetupGuide.md)

Starting with newly-commissioned Windows machine:

* Install git.
* Setup SSH and register public SSH key with GitLab.
* Set PowerShell to execute unsigned remote scripts.
* Run PowerShell script `install-all-buildtools.ps1`.
* Clone the repository.
* Build.

## Setting Up

Run powershell with administrative privileges, then set powershell execution
policy so it can run scripts:

    set-executionpolicy remotesigned

For quick preliminary setup, there is a single script that installs all build
tools: [install-all-buildtools.ps1](setup/install-all-buildtools.ps1)

Details on individual components are given in sections that follow.

### Git

Script: [git-install.ps1](setup/git-install.ps1)
Repository Url: [git-install](setup/git-install.ps1)
Source Url: [https://github.com/tomlarse/Install-Git](https://github.com/tomlarse/Install-Git)
Destination: C:\Program Files\Git

### Visual Studio 2019

Only the native desktop workload is required, with all recommended components (Microsoft.VisualStudio.Workload.NativeDesktop).

No license key is required for the initial 90 day free trial. The command line setup can be adjusted to accommodate a license key.

Script: [visualstudio.ps1](setup/visualstudio.ps1)
Download Url: [https://download.visualstudio.microsoft.com/download/pr/308e891b-f15e-43d8-8cc1-0e41f4962d4b/3d9dcb2b792f2f8e4de13e1e7f0bec7c2d44998c7892509b8044e12dfc63ea39/vs\_Professional.exe](https://download.visualstudio.microsoft.com/download/pr/308e891b-f15e-43d8-8cc1-0e41f4962d4b/3d9dcb2b792f2f8e4de13e1e7f0bec7c2d44998c7892509b8044e12dfc63ea39/vs_Professional.exe)
Release History: [https://docs.microsoft.com/en-us/visualstudio/releases/2019/history#release-dates-and-build-numbers](https://docs.microsoft.com/en-us/visualstudio/releases/2019/history%23release-dates-and-build-numbers)

### Windows SDK 1903

There may be issues with the SDK provided by Visual Studio, so it is necessary
to install separately.

**NOTE**: The presence of Windows 10 SDK versions newer than 10.0.20348.0 may
cause the following error:
```
SignTool error : No file digest algorithm specified. Please specify the digest
algorithm with the /fd flag. Using /fd SHA256 is recommended and more secure
than SHA1. Calling signtool with /fd sha1 is equivalent to the previous
behavior. In order to select the hash algorithm used in the signing
certificate's signature, use the /fd certHash option.
```

Script: [winsdk.ps1](setup/winsdk.ps1)
Download Url: [https://go.microsoft.com/fwlink/?linkid=2083338](https://go.microsoft.com/fwlink/?linkid=2083338)

### .Net 3.5

.Net 3.5 is required by Wix. Execute following powershell command:
```
Enable-WindowsOptionalFeature -Online -FeatureName "NetFx3" -All
```

### Wix 3.11

Script: [wix.ps1](setup/wix.ps1)
Destination: C:\Program Files (x86)\WiX Toolset v3.11
Download Url: [https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311.exe](https://github.com/wixtoolset/wix3/releases/download/wix3112rtm/wix311.exe)

### Python 2.7

Python is required for the main build tool: "scons".
This needs to be added to the front of the environment path.

Script: [python.ps1](setup/python.ps1)
Destination: C:\Python27
Download Url: [https://www.python.org/ftp/python/2.7.18/python-2.7.18.msi](https://www.python.org/ftp/python/2.7.18/python-2.7.18.msi)

### Scons 1.3.1

Install pywin32 first.

Script: [scons.ps1](setup/scons.ps1)
Destination: C:\scons-1.3.1
Download Url: [https://sourceforge.net/projects/scons/files/scons/1.3.1/scons-1.3.1.zip/download](https://sourceforge.net/projects/scons/files/scons/1.3.1/scons-1.3.1.zip/download)

### Go (1.14.6)

Script [go.ps1](setup/go.ps1)
Destination: C:\Go
Download Url: [https://storage.googleapis.com/golang/go1.14.6.windows-amd64.msi](https://storage.googleapis.com/golang/go1.14.6.windows-amd64.msi)

### PSTools

Script [pstools.ps1](setup/pstools.ps1)
Destination: C:\Program Files (x86)\pstools
Download Url: [https://download.sysinternals.com/files/PSTools.zip](https://download.sysinternals.com/files/PSTools.zip)

### System Environment Path and Variables

The system path has to be augmented to include python, and an Omaha-specific
environment variable for the python directory needs to be made pervasive.

Script: [environment.ps1](setup/environment.ps1)

## Getting The Code

To clone the
[Omaha Build repository](https://gitlab.com/Distributed-Compute-Protocol/omaha-build),
enter the following:
```
git clone --recursive git@gitlab.com:Distributed-Compute-Protocol/omaha-build.git
```

## Building

To build, enter the following:
```
mkdir build
cd build
cmake .. [-D OMAHA_ROOT=<omaha-source-directory>]
cmake --build . --target Omaha-library --config <build-configuration>
```

Where:

* `OMAHA_ROOT` specifies a local clone of the Omaha repository. If not provided,
  it will be automatically downloaded into
  `<build-directory>/src/omaha-library`.

## Testing

After building, run the tests by entering the following as an Administrator in
the build directory:
```
cmake --build . --target omaha-check
```

## Deploying

To deploy, clone
[dcp-native-ci](https://gitlab.com/Distributed-Compute-Protocol/dcp-native-ci)
and read the "Deploying" section of the "README.md" file therein.
